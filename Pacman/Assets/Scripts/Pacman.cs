using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//==========================================================================================
// Last Update: 2022-05-22
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to control the main player character Bac-Man.
// Initially authored by Joshua Ferguson
// Amendments made by Buket Swain
//==========================================================================================

public class Pacman : MonoBehaviour
{
    public GameObject mapPanel;

    //Serialized variables
    [SerializeField] private int lives = 3;
    [SerializeField] private float invincibleTime = 3;
    [SerializeField] private float speed = 3;
    [SerializeField] private Transform pacmanSpawn;
    [SerializeField] private GameObject[] lifeIcons;
    [SerializeField] private AudioClip deathClip;
    [SerializeField] private GameObject shield;

    //Private variables
    private int currentLives = 0;
    private float respawnTimer = -1;
    private Vector2 input;
    private CharacterController controller;
    private AudioSource aSrc;

    /// <summary>
    /// Creates necessary references.
    /// Prompts for missing references.
    /// </summary>
    private void Awake()
    {
        //Find controller reference
        TryGetComponent(out CharacterController charController);
        if (charController != null)
        {
            controller = charController;
        }
        else
        {
            Debug.LogError("Pacman: Character controller required.");
        }

        //Find audio source
        TryGetComponent(out AudioSource audioSource);
        if(audioSource != null)
        {
            aSrc = audioSource;
        }
        else
        {
            Debug.LogError("Pacman: Audio source required.");
        }

        //Check for trigger collider
        bool colliderFound = false;
        foreach (Collider col in GetComponents<Collider>())
        {
            if(col.isTrigger == true)
            {
                colliderFound = true;
                break;
            }
        }
        if(colliderFound == false)
        {
            Debug.LogError("Pacman: Collider with 'isTrigger' set to true is required.");
        }
    }

    /// <summary>
    /// Set initial game state.
    /// </summary>
    private void Start()
    {
        // (B.S.) Set Shield to not visible 
        shield.SetActive(false);

        //Set lives
        currentLives = lives;
        for (int i = 0; i < lifeIcons.Length; i++)
        {
            if (lifeIcons[i] != null)
            {
                if (i < currentLives)
                {
                    lifeIcons[i].SetActive(true);
                }
                else
                {
                    lifeIcons[i].SetActive(false);
                }
            }
        }
        //Victory method assigned to event // This is an anonymous method
        GameManager.Instance.Event_GameVictory += delegate { enabled = false; };  // (B.S.) This turns of Bac-man. Took me all evening to figure that out!! Arrgh

    }

    /// <summary>
    /// Frame by frame functionality.
    /// </summary>
    void Update()
    {
        //Respawn timer
        if(respawnTimer > -1)
        {
            //Debug.Log("I am strong");
            respawnTimer += Time.deltaTime;

            //(B.S) Check if 90% of the time is up
            if (respawnTimer > invincibleTime * 0.9)
            {
                // (B.S.) invincible time is ALMOST up so turn off shield This gives time to get away
                shield.SetActive(false);
                if (respawnTimer > invincibleTime)
                {
                    // (B.S.) Reset timer
                    respawnTimer = -1;
                }
            }
        }

        //Read inputs
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");
        Vector3 motion = Vector3.zero;
        //Left/right movement
        if (input.x > 0)
        {
            transform.forward = Vector3.right;
            motion = transform.forward.normalized;
        }
        if (input.x < 0)
        {
            transform.forward = -Vector3.right;
            motion = transform.forward.normalized;
        }
        //Forward/backward movement
        if (input.y > 0)
        {
            transform.forward = Vector3.forward;
            motion = transform.forward.normalized;
        }
        if (input.y < 0)
        {
            transform.forward = -Vector3.forward;
            motion = transform.forward.normalized;
        }


        // Create a temporary reference to the current scene.
        //Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        //string sceneName = currentScene.name;
        //Debug.Log(SceneManager.GetActiveScene().name);

        //Apply movement to controller if the main map is not active
        if (mapPanel.activeSelf == false)
        {
            controller.Move(motion.normalized * speed * Time.deltaTime);
        }


        //Bac-man is allowed to move in the Retro scene in map view
        if(mapPanel.activeSelf == true && SceneManager.GetActiveScene().name == "RetroGameScene")
        {
            controller.Move(motion.normalized * speed * Time.deltaTime);
        }
    }

    /// <summary>
    /// World interactions.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //Detect collisions with different objects
        //Debug.Log("Trigger is: " + other.tag);
        switch (other.tag)
        {
            case "Pellet":
                GameManager.Instance.PickUpPellet(1);
                other.gameObject.SetActive(false);
                break;
            case "Power Pellet":
                GameManager.Instance.PickUpPellet(1, 1);
                other.gameObject.SetActive(false);
                break;
            case "Bonus Item":
                GameManager.Instance.PickUpPellet(50, 2);
                other.gameObject.SetActive(false);
                break;
            case "Ghost":
                if (GameManager.Instance.PowerUpTimer > -1)
                {
                    other.TryGetComponent(out Ghost ghost);
                    if (ghost.CurrentState != ghost.RespawnState)
                    {
                        GameManager.Instance.EatGhost(ghost);
                        Debug.Log("Eat a ghost");
                    }
                }
                else 
                { 
                    Die(); 
                }
                break;
        }
    }

    /// <summary>
    /// Respawns the player.
    /// </summary>
    private void RespawnPlayer()
    {
        //Move player to spawn
        controller.enabled = false;
        transform.position = pacmanSpawn.position;
        transform.forward = pacmanSpawn.forward;
        controller.enabled = true;
        respawnTimer = 0;
        shield.SetActive(true);
    }

    /// <summary>
    /// Subtracts player lives.
    /// </summary>
    /// <returns></returns>
    public bool Die()
    {
        if (respawnTimer == -1)
        {
            //Subtract life
            currentLives--;

            aSrc.PlayOneShot(deathClip);

            if (currentLives > 0)                   
            {
                
                Debug.Log("Current: " + currentLives);
                if (currentLives < lifeIcons.Length)
                {
                    lifeIcons[currentLives].SetActive(false);
                }
                else
                {
                    Debug.LogError("There are fewer life icons then the player's current lives.");
                }
                RespawnPlayer();
                return false;
            }
            else    //Game over
            {
                lifeIcons[currentLives].SetActive(false); // (B.S.) turn off the last bread icon this doesn't adhere to DRY very well but I am running out of time to be effcient
                Debug.Log("Is it game over?");
                enabled = false;

                GameManager.Instance.gameEnd = true;
                GameManager.Instance.Delegate_GameOver.Invoke();
                return true;
            }
        }
        return false;
    }
}
