using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//==========================================================================================
// Last Update: 2022-05-19
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed by Buket Swain to manage the main overview map. 
//==========================================================================================

public class MainMap : MonoBehaviour
{
    public GameObject mainMapPanel;
    public CharacterController bacman;
    
    //Set singleton
    public static MainMap Instance { get; private set; }

    private void Awake()
    {
        //Set singleton
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Make sure the map panel is turned off at the start unless the game is in Retro mode.
        if (SceneManager.GetActiveScene().name == "RetroGameScene")
        {
            mainMapPanel.SetActive(true);
        }
        else 
        {
            mainMapPanel.SetActive(false);
        } 
    }

    // Update is called once per frame
    void Update()
    {
        //check if the M or m key is pressed
        //if so display the main map
        if (SceneManager.GetActiveScene().name == "RetroGameScene")
        {
            if(GameManager.Instance.gameEnd == true || GameManager.Instance.gameWon == true)
            {
                mainMapPanel.SetActive(false);
            }
            else
            {
                mainMapPanel.SetActive(true);
            }
            
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                if (GameManager.Instance.gameWon == false && GameManager.Instance.gameEnd == false)
                {
                    //Debug.Log("Press M");
                    mainMapPanel.SetActive(!mainMapPanel.activeSelf);
                }
            }
        }
    }
}
