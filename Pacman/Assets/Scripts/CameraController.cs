using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//==========================================================================================
// Last Update: 2022-05-10
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to manage the main camera.
// Initially authored by Joshua Ferguson
// Amendments made by Buket Swain
//==========================================================================================

public class CameraController : MonoBehaviour
{
    //Serialized variables
    [SerializeField] private float speed;
    [SerializeField] private Vector3 offset;

    //Private variables
    private Transform target;
    private Vector3 offsetVector;

    /// <summary>
    /// Find necessary references
    /// Target of the camera is the object with the 'Player' tag.
    /// </summary>
    private void Awake()
    {
        //Find target
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    /// <summary>
    /// Late frame by frame functionality.
    /// </summary>
    void LateUpdate()
    {
        if(target != null)
        {
            //Calculate desired position
            offsetVector = target.position + offset;
            if (Vector3.Distance(transform.position, offsetVector) > 0.1f)
            {
                //Calculate movement
                Vector3 motion = (offsetVector - transform.position);
                if (motion.magnitude < 2 || motion.magnitude > 4)
                {
                    motion = motion.normalized * motion.magnitude * speed * Time.deltaTime;
                }
                else
                {
                    motion = motion.normalized  * speed * Time.deltaTime;
                }
                //Apply movement to object
                transform.position += motion;
            }
                        
            // (B.S.) Locked the rotation of the camera to the player because otherwise I was getting confused with the directions!!
            //transform.rotation = target.rotation;

            //Rotate toward target
            transform.LookAt(target);
        }
        else
        {
            Debug.LogError("Camera Controller: Camera target must be tagged as 'Player'!");
        }
    }
}