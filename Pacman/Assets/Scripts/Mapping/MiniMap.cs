using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//==========================================================================================
// Last Update: 2022-05-10
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to manage the main camera.
// Initially authored by Joshua Ferguson
// This script has been developed by Buket Swain to manage the mini map. 
//==========================================================================================

public class MiniMap : MonoBehaviour
{
    //Serialized variables
    [SerializeField] private float speed;
    [SerializeField] private Vector3 offset;

    //Private variables
    private Transform target;
    private Vector3 offsetVector;

    /// <summary>
    /// Find necessary references
    /// Target of the camera is the object with the 'Player' tag.
    /// </summary>
    private void Awake()
    {
        //Find target
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    /// <summary>
    /// Late frame by frame functionality.
    /// </summary>
    private void Update()
    {
        //Make sure there is a targer which is the player bac-man
        if (target != null)
        {
            //Use an offset to place the camera above the player looking down
            offsetVector = target.position + offset;
            transform.position = offsetVector;
        }
        else
        {
            Debug.LogError("Camera Controller: Camera target must be tagged as 'Player'!");
        }
    }
}