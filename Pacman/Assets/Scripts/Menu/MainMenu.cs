using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//==========================================================================================
// Last Update: 2022-05-22
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed by Buket Swain to manage the main menu in the game. 
// It performs the functionality of the main menu buttons.
// - Start game
// - Toggle game keys
// - Toggle credits
// - Quit application
//==========================================================================================

public class MainMenu : MonoBehaviour
{
    public GameObject keysPanel;
    public GameObject howtoplayPanel;

    /// <summary>
    /// When the scene first loads makes sure the
    /// panels are toggled off
    /// </summary>
    private void Awake()
    {
        //Checks if the Panels are already active, if so calls a function to toggle it off.

        if (keysPanel.activeSelf == true)
        {
            ToggleKeysWindow();
        }

        if (howtoplayPanel.activeSelf == true)
        {
            ToggleHowToPlayWindow();
        }
    }

    /// <summary>
    /// This function starts the Game by loading the game scene. 
    /// </summary>
    public void StartGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// This function starts the Game by loading the game scene. 
    /// </summary>
    public void StartRetroGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }

    /// <summary>
    /// Toggles Key  panel between visible and not visible
    /// </summary>
    public void ToggleKeysWindow()
    {
        keysPanel.SetActive(!keysPanel.activeSelf);
        howtoplayPanel.SetActive(false);
    }

    /// <summary>
    /// Toggles Credits panel between visible and not visible
    /// </summary>
    public void ToggleHowToPlayWindow()
    {
        howtoplayPanel.SetActive(!howtoplayPanel.activeSelf);
        keysPanel.SetActive(false);
    }

    /// <summary>
    /// This function quits the whole Application. 
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}
