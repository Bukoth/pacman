using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//==========================================================================================
// Last Update: 2022-05-12
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to manage the AI named Pinky's behaviour.
// Initially authored by Joshua Ferguson
// No amendments made by Buket Swain
//==========================================================================================

public class Pinky : Ghost
{
    [SerializeField] private Vector3 offset;

    protected override void Awake()
    {
        base.Awake();
        DefaultState = new GhostState_Flank(this, offset);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    private void OnDrawGizmos()
    {
        if (Target != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(Target.transform.position + offset, 0.25f);
        }
    }
}
