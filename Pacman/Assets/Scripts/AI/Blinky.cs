using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//==========================================================================================
// Last Update: 2022-05-12
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to manage the AI named Blinky's behaviour.
// Initially authored by Joshua Ferguson
// No amendments made by Buket Swain
//==========================================================================================

public class Blinky : Ghost
{
    protected override void Awake()
    {
        base.Awake();
        DefaultState = new GhostState_Chase(this);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }
}