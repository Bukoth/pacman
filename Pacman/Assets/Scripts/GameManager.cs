using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//==========================================================================================
// Last Update: 2022-05-10
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed to manage the overall game.
// Initially authored by Joshua Ferguson
// Amendments made by Buket Swain
//==========================================================================================

public class GameManager : MonoBehaviour
{
    //Serialized variables
    //[SerializeField] private GameObject pelletPrefab;         //Added by B.S. TO GENERATE PELLETS FROM PREFABS then decided not to autogenerate
    [SerializeField] private float powerUpTime = 10;
    [SerializeField] private Text scoreText;
    [SerializeField] private Transform bonusItemSpawn;
    [SerializeField] private Bounds ghostSpawnBounds;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private Text endPanelText;
    [SerializeField] private Text endPanelScoreText;
    [SerializeField] private AudioClip pelletClip;
    [SerializeField] private AudioClip powerPelletClip;
    [SerializeField] private AudioClip bonusItemClip;
    [SerializeField] private AudioClip eatGhostClip;

    public bool gameWon = false;
    public bool gameEnd = false;

    //Private variables
    private GameObject bonusItem;
    private int totalPellets = 176;     //CHANGED TOTAL PELLETS FROM 0 TO 10 (B.S.) needs to be (totalpellets + 4 power ups) divisble by 4 for the bonus thing to work
    private int score = 0;
    private int collectedPellets = 0;
    private AudioSource aSrc;

    //Auto-properties
    public float PowerUpTimer { get; private set; } = -1;
    public Bounds GhostSpawnBounds { get { return ghostSpawnBounds; } }

    //Singletons
    public static GameManager Instance { get; private set; }

    //Delegates
    public delegate void PowerUp();
    public delegate void GameEvent();
    public GameEvent Delegate_GameOver = delegate { };

    //Events
    public event PowerUp Event_PickUpPowerPellet = delegate { };
    public event PowerUp Event_EndPowerUp = delegate { };
    public event GameEvent Event_GameVictory = delegate { };

    /// <summary>
    /// Create necessary references.
    /// </summary>
    private void Awake()
    {
        //Set singleton
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Game Manager: More than one Game Manager is present in the scene.");
            enabled = false;
        }


        //Set audio source reference
        TryGetComponent(out AudioSource audioSource);
        if(audioSource != null)
        {
            aSrc = audioSource;
        }
        else
        {
            Debug.LogError("Game Manager: No audio source attached to Game Manager!");
        }

        // DECIDED NOT TO GENERATE PELLETS AUTOMATICALLY
        ////---------- Generate pellets automatically (B.S)
        //Vector3 spawnPosition = new Vector3(1, 1, 1);
        //int xOffset = 0;
        //int zOffset = 0;
        

        //for (int i=0; i < totalPellets; i++)
        //{
        //    xOffset += 2;
        //    zOffset += 2;
        //    Vector3 pelletOffset = new Vector3(0, 0, zOffset);
        //    GameObject pellet = Instantiate(pelletPrefab, spawnPosition + pelletOffset, transform.rotation);
        //}
        ////-- end generate pellets

        //Find bonus item
        bonusItem = GameObject.FindGameObjectWithTag("Bonus Item");
        //Count pellets
        //totalPellets = GameObject.FindGameObjectsWithTag("Pellet").Length;  // COMMENTED THIS OUT BECAUSE I WILL GENERATE MY PELLETS (B.S.)
        totalPellets += GameObject.FindGameObjectsWithTag("Power Pellet").Length;

        //Debug.Log("Total Pellets: " + totalPellets);
    }

    /// <summary>
    /// Set initial game state.
    /// </summary>
    private void Start()
    {
        //Assign delegates/events
        Event_GameVictory += ToggleEndPanel;
        Delegate_GameOver += ToggleEndPanel;

        //Disable bonus item
        if (bonusItem != null)
        {
            bonusItem.SetActive(false);
        }
        else
        {
            Debug.LogError("Game Manager: Bonus item must be in the scene and tagged as 'Bonus Item'!");
        }

        //Disable end game panel
        if (endPanel != null)
        {
            if (endPanel.activeSelf == true)
            {
                ToggleEndPanel();
            }
        }
        else
        {
            Debug.LogError("Game Manager: End Panel has not been assigned!");
        }

        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $" {score} ";
        }
        else
        {
            Debug.LogError("Game Manager: Score Text has not been assigned!");
        }
    }

    /// <summary>
    /// Frame by frame functionality.
    /// </summary>
    void Update()
    {
        //Active power up timer
        if(PowerUpTimer > -1)
        {
            PowerUpTimer += Time.deltaTime;
            if(PowerUpTimer > powerUpTime)  //Power up timer finished
            {
                Event_EndPowerUp.Invoke();
                PowerUpTimer = -1;
            }
        }
    }

    /// <summary>
    /// Called when a pellet is picked up.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="powerUp"></param>
    /// <param name="bonus"></param>
    public void PickUpPellet(int value, int type = 0)
    {
        //Add score
        score += value;

        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $" {score} ";
        }
        else
        {
            Debug.LogError("Game Manager: Score Text has not been assigned!");
        }

        if(type == 0) //Normal pellet
        {
            aSrc.PlayOneShot(pelletClip);
        }
        else if (type == 1) //Activate power up
        {
            Event_PickUpPowerPellet.Invoke();
            PowerUpTimer = 0;
            aSrc.PlayOneShot(powerPelletClip);
        }

        if (type != 2)
        {
            collectedPellets++;
            //Debug.Log("Total pellets: " + totalPellets);
            //Check ratio of collected pellets
            float ratio = (float)collectedPellets / totalPellets;
            //Debug.Log("Ratio: " + ratio);

            if (ratio != 1)
            {
                if (ratio % 0.25f == 0)
                {
                    //Spawn in bonus item
                    if (bonusItem != null)
                    {
                        if (bonusItem.activeSelf == false)
                        {
                            if (bonusItemSpawn != null)
                            {
                                bonusItem.transform.position = bonusItemSpawn.position;
                                bonusItem.SetActive(true);
                            }
                            else
                            {
                                Debug.LogError("Game Manager: Bonus Item Spawn has not been assigned!");
                            }
                        }
                    }
                    else
                    {
                        Debug.LogError("Game Manager: Bonus item must be in the scene and tagged as 'Bonus Item'!");
                    }
                }
            }
            else
            {
                gameWon = true;
                Event_GameVictory.Invoke();
            }
        }
        else
        {
            aSrc.PlayOneShot(bonusItemClip);
        }
    }

    /// <summary>
    /// Called when a ghost is eaten.
    /// </summary>
    /// <param name="ghost"></param>
    public void EatGhost(Ghost ghost)
    {
        //Add score
        score += 5;
        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $"Score: {score}";
        }
        aSrc.PlayOneShot(eatGhostClip);
        //Respawn
        ghost.SetState(ghost.RespawnState);
    }

    // RestartGame() here has been deleted by (B.S.) as this function now happens in the custom PauseMenu.cs

    // QuitToDesktop() here has been deleted by (B.S.) as this function now happens in the custom PauseMenu.cs

    /// <summary>
    /// Toggles the end game panel on and off.
    /// </summary>
    private void ToggleEndPanel()
    {
        if (endPanel.activeSelf == false)
        {
            endPanel.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            if (gameWon == true) 
            {
                // (B.S.) Game win conditions displays game win message
                endPanelText.text = "You win!";
                endPanelScoreText.text = $"You scored {score} points";
                Time.timeScale = 0f;
            }
            else 
            {
                // (B.S.) Game lose conditions displays game win message
                MainMap.Instance.mainMapPanel.SetActive(false); // (B.S.) Trn off map so restart menu can be visible
                endPanelText.text = "You lose!";
                Time.timeScale = 0f;
            }
        }
        else
        {
            endPanel.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(ghostSpawnBounds.center, ghostSpawnBounds.size);
    }
}
