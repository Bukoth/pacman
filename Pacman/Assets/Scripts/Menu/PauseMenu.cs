using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//==========================================================================================
// Last Update: 2022-05-17
// Author in Project: Buket Swain
// Date: Semester 1 - 2022
// Lecturer: Joshua Ferguson
// Cluster: 3D Game Development
// Script Description:
// This script has been developed by Buket Swain to manage the pause menu in the main game scene. 
// It performs the functionality of the main meu buttons.
// - Pause game
// - Restart game
// - Go back to the main menu
//==========================================================================================

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;
    public GameObject pauseMenuUI;
    public Button resumeButton;
    public Button restartButton;
    public Button menuButton;
    public GameObject hudCanvas;

    //Singletons
    public static PauseMenu Instance { get; private set; }

    private void Awake()
    {
        //Set singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }
    //At the start of the game, makesure the pause menu is not active (visible)
    //We do not want the pause menu when the game first starts
    void Start()
    {
        pauseMenuUI.SetActive(false);
        gameIsPaused = false;
    }

    void Update()
    {
        //check if the Escape key is pressed
        //if so and if game is already paused resume
        //if game is not paused pause the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(GameManager.Instance.gameWon == false && GameManager.Instance.gameEnd == false)
            {
                //Toggle Pause Menu
                if (gameIsPaused == true)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }

    /// <summary>
    /// Inactivate the pause menu when the game resumes
    /// Set the time back to normal flow of time so the timer continues to count
    /// set the game state to not paused.
    /// Disable the cursor.
    /// Enable the player and the mouse
    /// </summary>
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        hudCanvas.SetActive(true);
    }

    /// <summary>
    /// Activate the pause menu when the game resumes
    /// Set the time to freeze to stop timer counting
    /// set the game state to paused.
    /// Enable the cursor.
    /// Disable the player and the mouse
    /// </summary>
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        hudCanvas.SetActive(false);
    }

    /// <summary>
    /// Load the main menu of the game
    /// Turn on time
    /// </summary>
    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Restarts the game
    /// Turn on time
    /// </summary>
    public void Restart()
    {
        Time.timeScale = 1f;
        gameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        hudCanvas.SetActive(true);
    }
}
